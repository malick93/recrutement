# -*- coding: utf-8 -*-
"""
Created on Thu Nov 24 15:58:08 2022

@author: Malick Thiam
"""

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import sklearn as sk
import seaborn as sb
# importantion des données
df=pd.read_csv("C:\\Users\\Malick Thiam\\Desktop\\doc\\data.csv",decimal=",")
#df.head(5)

# convertir les chiffres dans le tableau en float et remplacer les ',' par des '.'
df['Experience']= df['Experience'].astype(str).astype(float)

# 1) nombres d'observations et VM
print("Dans cette base de données il y a {0:d} observations.\n".format(df.shape[0]))
#9582 observations
print("La somme totale des valeurs manquantes de cette base est: ", df.isnull().sum().sum())
# 130 valeurs manquantes

#2) Imputation des VM pour la variable experience selon le métier
ds = df['Metier'] == 'Data scientist' 
de = df['Metier'] == 'Data engineer'
print("La somme des valeurs manquantes de la variable Expérience pour le métier Data Scientist avant imputation est :"+str((df.loc[ds,'Experience']).isna().sum()))
print("La somme des valeurs manquantes de la variable Expérience pour le métier Data Engineer avant imputation est:"+str((df.loc[de,'Experience']).isna().sum()))
# remplir les valeurs manquantes des Data Engineers par la valeur moyenne
mean_de = df[de]['Experience'].dropna().mean()
df.loc[de, 'Experience'] = df.loc[de, 'Experience'].fillna(mean_de)
# remplir les valeurs manquantes de Data Scientists par la valeur médiane
median_ds = df[ds]['Experience'].dropna().median()
df.loc[ds, 'Experience'] = df.loc[ds, 'Experience'].fillna(median_ds)
print("La somme des valeurs manquantes de la variable Expérience pour le métier Data Scientist aprés imputation est :"+str((df.loc[ds,'Experience']).isna().sum()))
print("La somme des valeurs manquantes de la variable Expérience pour le métier Data Engineer aprés imputation est :"+str((df.loc[de,'Experience']).isna().sum()))


# 3)  Années d'expériences en moyenne
# remplir les valeurs manquantes de Lead Data Scientist par la valeur moyenne
lds = df['Metier'] == 'Lead data scientist'
mean_lds = df[lds]['Experience'].dropna().mean()
df.loc[lds, 'Experience'] = df.loc[lds, 'Experience'].fillna(mean_lds)
# remplire les valeurs manquantes de Data Architecte par la valeur moyenne
da = df['Metier'] == 'Data architecte'
mean_da = df[da]['Experience'].dropna().mean()
df.loc[da, 'Experience'] = df.loc[da, 'Experience'].fillna(mean_da)
# remplire les valeurs manquantes de Data Scientist par la valeur moyenne
mean_ds= df[ds]['Experience'].dropna().mean()
df.loc[ds, 'Experience'] = df.loc[ds, 'Experience'].fillna(mean_ds)
#Affichge
print("Les années d'experiences en moyenne pour chaque métier:\n")
print("Lead data scientist : ", mean_lds)
print("Data architecte : ", mean_da)
print("Data enginner : ", mean_de)
print("Data scientist  : ", mean_ds)

#4 )  représentation graphique : diag en bar
plt.figure()
x=df['Metier'].value_counts().keys()
y=[mean_ds, mean_de, mean_lds, mean_da]
plt.bar(x,y,color=(0.2, 0.4, 0.6, 0.6))
plt.xticks(x,rotation=90)
plt.ylabel("La valeur moyenne\n")
plt.title("Le nombre moyen d'années d'expériences pour chaque métier\n")
plt.show()

#5) varible categorielle
df['Exp_label']= pd.cut(df.Experience,bins=4 , 
                labels=['Debutant','Confirme','Avance','Expert'])
df.head(5)

#6) top 5 technologie  les plus utilisées
tech = df['Technologies'].value_counts()[0:5]
print(tech)
# Affichage graphique
ax = tech.plot(kind='barh', figsize=(10,7))
ax.invert_yaxis()
plt.xlabel("\nProfile")
plt.title('Le top 5 des technologies les plus utilisés')

# 7) Réaliser une méthode de clustering non supervisée
from sklearn import preprocessing
from sklearn.cluster import KMeans
from sklearn.preprocessing import MinMaxScaler

# codons les chaines de caractetères de la base pour pouvoir mener le calculs 
le = preprocessing.LabelEncoder()
df_enc = df.loc[:,['Experience']]
df_enc['Ville_enc'] = pd.Series(le.fit_transform(np.squeeze(df.loc[:,['Ville']])[:]), index = df_enc.index)
df_enc['Technologies_enc'] = pd.Series(le.fit_transform(np.squeeze(df.loc[:,['Technologies']])[:]), index = df_enc.index)
df_enc['Diplome_enc'] = pd.Series(le.fit_transform(np.squeeze(df.loc[:,['Diplome']])[:]), index = df_enc.index)
df_enc['Exp_label_enc'] = pd.Series(le.fit_transform(np.squeeze(df.loc[:,['Exp_label']])[:]), index = df_enc.index)
df_enc = np.round(df_enc,2)
df_enc.head(5)

## 7.a) 
# La méthode choisi pour le clustering est la méthode K-Means,
# pour sa simplicité et la connaissance du nombre de cluster (2)
X = df_enc.astype(float)
scaler = MinMaxScaler()
X_scaled = scaler.fit_transform(X)

kmeans = KMeans(n_clusters=2, max_iter = 100, algorithm = 'auto')
kmeans.fit(X_scaled)
labels_ = kmeans.labels_
# Evaluation de l'erreur de clustering
c1, c2 = kmeans.cluster_centers_

# 7.b)
# La métrique : erreur moyenne quadratique normalisée
# Evaluation de l'erreur de clustering
NMSE = lambda a,b: 100 * ((a-b)**2).sum() / ((a)**2).sum()
NMSE1 = NMSE(X_scaled[labels_==0], c1) 
NMSE2 = NMSE(X_scaled[labels_==1], c2)
print(' NMSE  cluster 1: {0:2.2f}%'.format(NMSE1))
print(' NMSE  cluster 2: {0:2.2f}%'.format(NMSE2))
# Les erreurs moyennes quadratiques normalisées pour les deux clusters sont moins de 20%

# affichage resultats
plt.figure(figsize=(20,30))
plt.subplot(521),
ax = df_enc.loc[labels_==1,'Experience'].plot(kind='hist', color='green', label='cluster_1')
plt.legend()
plt.xlabel("Experience")
plt.subplot(522),
ax = df_enc.loc[labels_==0,'Experience'].plot(kind='hist', color='purple', label='cluster_2')
plt.xlabel("Experience")
plt.legend()

plt.subplot(523),
ax = df.loc[labels_==1,'Diplome'].value_counts().plot(kind='bar', color='green', label='cluster_1')
plt.xlabel("Diplome")
plt.ylabel("Profile")
plt.legend()
plt.subplot(524),
ax = df.loc[labels_==0,'Diplome'].value_counts().plot(kind='bar', color='purple', label='cluster_2')
plt.xlabel("Diplome")
plt.ylabel("Profile")
plt.legend()

plt.subplot(513),
ax = df.loc[labels_==0,'Ville'].value_counts().plot(kind='bar', color = 'purple', label='cluster_2')
ax = df.loc[labels_==1,'Ville'].value_counts().plot(kind='bar', color = 'green', label='cluster_1')
plt.ylabel("Profile")
plt.xlabel("Ville")
plt.legend()

plt.subplot(514),
ax = df_enc.loc[labels_==0,'Technologies_enc'].value_counts()[0:5].plot(kind='bar', color = 'purple', label='cluster_2')
ax = df_enc.loc[labels_==1,'Technologies_enc'].value_counts()[0:5].plot(kind='bar', color = 'green', label='cluster_1')
plt.ylabel("Profile")
plt.xlabel("\n Technologie")
plt.legend()

plt.subplot(529)
ax=df.loc[labels_==1,'Exp_label'].value_counts().plot(kind='bar', color='green', label='cluster_1')
plt.ylabel("Profile")
plt.xlabel("\n Experience Label")
plt.legend()
plt.subplot(5,2,10)
ax=df.loc[labels_==0,'Exp_label'].value_counts().plot(kind='bar', color='purple', label='cluster_2')
plt.ylabel("Profile")
plt.xlabel("\n Experience Label")
plt.legend()

plt.subplots_adjust(hspace=1)

# Les caracteristiques les plus imortantes des deux clusters sont le Diplome et l'Experience
# Première caractérstique est le Diplome: 
#Cluster 1 : Master et Bachelor
#Cluster 2 : No diplome et PhD
#Deuxième caractérstique est l'Expérience :
#Le nombre d'année d'Experience des profils du premier cluster
# est beaucoup plus élevés par rapport aux profils du deuxieme cluster

#8) prédiction des métiers manquants 
from sklearn import model_selection
from sklearn.linear_model import LogisticRegressionCV
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import classification_report, accuracy_score, confusion_matrix


metier = df.loc[:, 'Metier'].isna()
# Préparation de la base de donnée sans métier nulle
X = np.array(df_enc[~ metier])
y = le.fit_transform(df.loc[~ metier, 'Metier'].dropna())
#metier_na contient les donnée avec la caractéristique du metier est nulle.
metier_test = np.array(df_enc[metier])
#divisions des données 75% pour le train et 25% pour le test
X_train, X_test, y_train, y_test = model_selection.train_test_split(
    X, y, test_size=.25, shuffle=True, random_state=40)

# j'ai choisi l'algo regression logistique multi_class
clf = LogisticRegressionCV(cv=5, random_state=0, max_iter=400,
         multi_class='multinomial').fit(X_train, y_train)

# classifi arbre de decision
model1= DecisionTreeClassifier(max_depth=2).fit(X_train, y_train)
model_prediction = model1.predict(X_test)

y_predicted = clf.predict(X_test)
print('La précision de la classificiation est de: ' +
      str(np.round(accuracy_score(y_test, y_predicted), 2)*100)+'%   \n')
# la précision de 42% est plutot faible
# il faut peut etre utilisé autre algo: exemple le model des k- plus proches voisins
print('\nLe rapport de classificiation :\n')
print(classification_report(y_test, y_predicted))
# matrice de confusion
matrice_conf = confusion_matrix(y_test, y_predicted)
print('Matrice de confusion :\n')
print(matrice_conf)

#Prédicition les métiers manquants par notre modéle
Metier_predicted=model1.predict(metier_test)
#Remplissage de la base de données par les métiers prédis 
df.loc[~ metier, 'Metier_predicted'] = df.loc[:, 'Metier']
df.loc[metier, 'Metier_predicted'] = le.inverse_transform(Metier_predicted)
#Affichage
print("Le résultat de prediction des métiers manquants est obtenu à l'aide d'une classification des données avec une regression logistique multi-class:\n")
df[metier]
